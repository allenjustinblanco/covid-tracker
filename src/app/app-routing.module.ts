import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { StatesComponent } from './components/states/states.component';
import { HomeComponent } from './components/home/home.component';

const routes: Routes = [
  { path: 'covid-tracker/', component: HomeComponent},
  { path: 'covid-tracker/states', component: StatesComponent },
  { path: 'covid-tracker/home', component: HomeComponent}
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
