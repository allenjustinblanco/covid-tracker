import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Observable } from 'rxjs';
import { StateCase } from '../models/state-case.model';

const baseUrl = "https://corona.lmao.ninja/v2"

@Injectable({
  providedIn: 'root'
})
export class NovelCovidService {

  constructor(private http: HttpClient) { }

  getAll(): Observable<any[]> {
    return this.http.get<any[]>(`${baseUrl}/all?yesterday`);
  }

  getAllStates(): Observable<any[]> {
    return this.http.get<any[]>(`${baseUrl}/states?sort&yesterday`);
  }

  findByState(state: any): Observable<StateCase[]> {
    return this.http.get<StateCase[]>(`${baseUrl}/states/${state}?yesterday=true`);
  }
}
