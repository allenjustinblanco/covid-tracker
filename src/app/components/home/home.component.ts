import { Component, OnInit } from '@angular/core';
import { NovelCovidService } from 'src/app/services/novel-covid.service';

@Component({
  selector: 'app-home',
  templateUrl: './home.component.html',
  styleUrls: ['./home.component.css']
})
export class HomeComponent implements OnInit {

  global: any;

  constructor(private novelService: NovelCovidService) { }

  ngOnInit(): void {
    this.getGlobalCases();
  }

  getGlobalCases() {
      this.novelService.getAll()
        .subscribe(
          data => {
            this.global = data;
            console.log(data)
          },
          error => {
            console.log(error);
          }
        )
  }

}
