import { Component, OnInit } from '@angular/core';
import { NovelCovidService } from 'src/app/services/novel-covid.service';
import { StateCase } from 'src/app/models/state-case.model';

@Component({
  selector: 'app-states',
  templateUrl: './states.component.html',
  styleUrls: ['./states.component.css']
})
export class StatesComponent implements OnInit {

  states: StateCase[];
  currentState?: StateCase;
  currentIndex = -1;
  state = '';
  searchText = '';

  constructor(private novelService: NovelCovidService ) { }

  ngOnInit(): void {
    this.getStateCases();
  }

  getStateCases() {
    this.novelService.getAllStates()
      .subscribe(
        data => {
          this.states = data;
          console.log(data)
        },
        error => {
          console.log(error);
        }
      )
  }

  refreshList(): void {
    this.getStateCases();
    this.currentState = undefined;
    this.currentIndex = -1;
  }

  setActiveState(StateCase: StateCase, index: number): void {
    this.currentState = StateCase;
    this.currentIndex = index;
  }

  searchState(): void {
    this.currentState = undefined;
    this.currentIndex = -1;

    this.novelService.findByState(this.state)
      .subscribe(
        data => {
          this.states = data;
          console.log(data);
        },
        error => {
          console.log(error);
        });
  }

}

